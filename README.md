reference : https://medium.com/@porteneuve/how-to-make-git-preserve-specific-files-while-merging-18c92343826b

The key is to execute the following command in terminal first :
git config --global merge.ours.driver true

then create a .gitattributes file and fill it up with a value like this one :
/app/Config/database.php merge=ours

Then whenever your local /app/Config/database.php file is different with the one in your base repo, that local file will not be replaced in every git pull execution
